/*
Copyright (c) 2018-2022 Perinet GmbH
All rights reserved

This software is dual-licensed: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3 as
published by the Free Software Foundation. For the terms of this
license, see http://www.fsf.org/licensing/licenses/agpl-3.0.html,
or contact us at https://server.io/contact/ when you want license
this software under a commercial license.
*/

package lifecycle

import (
	"encoding/json"
	"log"
	"net/http"
	"syscall"
	"time"

	"gitlab.com/perinet/generic/lib/httpserver"
	"gitlab.com/perinet/generic/lib/httpserver/periHttp"
	"gitlab.com/perinet/generic/lib/httpserver/rbac"
	"gitlab.com/perinet/generic/lib/utils/filestorage"
	"gitlab.com/perinet/generic/lib/utils/intHttp"
	"gitlab.com/perinet/periMICA-container/apiservice/lifecycle/lifecycleStatus"
	"gitlab.com/perinet/periMICA-container/apiservice/node"
	"gitlab.com/perinet/periMICA-container/apiservice/node/nodeErrorStatus"
	"gitlab.com/perinet/periMICA-container/apiservice/node/nodeLifeState"
)

func PathsGet() []httpserver.PathInfo {
	return []httpserver.PathInfo{
		{Url: "/lifecycle", Method: httpserver.GET, Role: rbac.USER, Call: LifeCycle_Info_Get},
		{Url: "/lifecycle/config", Method: httpserver.GET, Role: rbac.ADMIN, Call: LifeCycle_Config_Get},
		{Url: "/lifecycle/config", Method: httpserver.PATCH, Role: rbac.ADMIN, Call: LifeCycle_Config_Set},
		{Url: "/lifecycle/firmware/production-image", Method: httpserver.PUT, Role: rbac.ADMIN, Call: LifeCycle_FirmwareProductionImage_Set},
		{Url: "/lifecycle/firmware/production-image", Method: httpserver.GET, Role: rbac.ADMIN, Call: LifeCycle_FirmwareProductionImage_Get},
		{Url: "/lifecycle/firmware/update-image", Method: httpserver.PUT, Role: rbac.ADMIN, Call: LifeCycle_FirmwareUpdateImage_Set},
		{Url: "/lifecycle/firmware/update-image", Method: httpserver.GET, Role: rbac.ADMIN, Call: LifeCycle_FirmwareUpdateImage_Get},
		{Url: "/lifecycle/firmware/soft-reboot", Method: httpserver.PATCH, Role: rbac.ADMIN, Call: LifeCycle_Reboot_Set},
		{Url: "/lifecycle/factory-reset", Method: httpserver.PATCH, Role: rbac.ADMIN, Call: LifeCycle_FactoryReset_Set},
	}
}

const (
	API_VERSION = "23"
)

type ServiceLifeCycle struct {
	StoragePath string
}

type LifeCycleConfig struct {
	State node.NodeLifeState `json:"state"`
}

type LifeCycleInfo struct {
	ApiVersion    json.Number          `json:"api_version"`
	State         node.NodeLifeState   `json:"state"`
	ErrorStatus   node.NodeErrorStatus `json:"error_status"`
	RebootCounter int                  `json:"reboot_counter"`
}

type LifeCycleStatus = lifecycleStatus.Enum

type LifeCycleOEMProductionFirmwareImageStatus struct {
	State LifeCycleStatus `json:"state"`
}

type LifeCycleUpdateFirmwareImageStatus struct {
	State LifeCycleStatus `json:"state"`
}

var defaultLifeCycleInfo = LifeCycleInfo{ApiVersion: API_VERSION, State: nodeLifeState.OEM, ErrorStatus: node.NodeErrorStatus(nodeErrorStatus.NO_ERROR), RebootCounter: 0}
var defaultLifeCycleConfig = LifeCycleConfig{State: nodeLifeState.OEM}
var lifeCycle_cache = defaultLifeCycleInfo
var lifeCycleConfig_cache = defaultLifeCycleConfig
var lifeCycleInfoPath = "/var/lib/apiservice-lifecycle/lifeCycleInfo.json"
var lifeCycleConfigPath = "/var/lib/apiservice-lifecycle/lifeCycleConfig.json"
var lifeCycleOEMProductionFirmwareStatusPath = "/var/lib/apiservice-lifecycle/lifeCycleOEMProductionFirmwareStatusStorage.json"
var lifeCycleUpdateFirmwareStatusPath = "/var/lib/apiservice-lifecycle/lifeCycleUpdateFirmwareStatusStorage.json"

var lifeCycleOEMProductionFirmwareImageStatus LifeCycleOEMProductionFirmwareImageStatus
var lifeCycleUpdateFirmwareImageStatus LifeCycleUpdateFirmwareImageStatus

func init() {
	SetServiceLifeCycle(ServiceLifeCycle{StoragePath: "/var/lib/apiservice-lifecycle"})
	lifeCycleOEMProductionFirmwareImageStatus.State = lifecycleStatus.IDLE
	lifeCycleUpdateFirmwareImageStatus.State = lifecycleStatus.IDLE
}

func SetServiceLifeCycle(serviceLifeCycle ServiceLifeCycle) {
	var err error

	if serviceLifeCycle.StoragePath != "" {
		lifeCycleInfoPath = serviceLifeCycle.StoragePath + "/lifeCycleInfo.json"
		lifeCycleConfigPath = serviceLifeCycle.StoragePath + "/lifeCycleConfig.json"
		lifeCycleOEMProductionFirmwareStatusPath = serviceLifeCycle.StoragePath + "/lifeCycleOEMProductionFirmwareStatusStorage.json"
		lifeCycleUpdateFirmwareStatusPath = serviceLifeCycle.StoragePath + "/lifeCycleUpdateFirmwareStatusStorage.json"
	}

	//load persistent data
	err = filestorage.LoadObject(lifeCycleInfoPath, &lifeCycle_cache)
	if err != nil {
		log.Println("SetServiceLifeCycle: lifeCycle load error: ", err)
	}

	err = filestorage.LoadObject(lifeCycleConfigPath, &lifeCycleConfig_cache)
	if err != nil {
		log.Println("SetServiceLifeCycle: lifeCycleConfig load error: ", err)
	}

	err = filestorage.LoadObject(lifeCycleOEMProductionFirmwareStatusPath, &lifeCycleOEMProductionFirmwareImageStatus)
	if err != nil {
		lifeCycleOEMProductionFirmwareImageStatus.State = lifecycleStatus.IDLE
		log.Println("SetServiceLifeCycle: lifeCycleOEMProductionFirmwareImageStatus load error: ", err)
	}

	err = filestorage.LoadObject(lifeCycleUpdateFirmwareStatusPath, &lifeCycleUpdateFirmwareImageStatus)
	if err != nil {
		lifeCycleUpdateFirmwareImageStatus.State = lifecycleStatus.IDLE
		log.Println("SetServiceLifeCycle: lifeCycleUpdateFirmwareImageStatus load error: ", err)
	}
}

func LifeCycle_Info_Get(p periHttp.PeriHttp) {
	// update state
	lifeCycle_cache.State = lifeCycleConfig_cache.State

	binaryJson, err := json.Marshal(lifeCycle_cache)
	if err != nil {
		p.JsonResponse(http.StatusInternalServerError, json.RawMessage(`{"error": "`+err.Error()+`"}`))
		return
	}

	p.JsonResponse(http.StatusOK, binaryJson)
}

func LifeCycle_FirmwareProductionImage_Set(p periHttp.PeriHttp) {
	// update status
	lifeCycleOEMProductionFirmwareImageStatus.State = lifecycleStatus.RECEIVING
	filestorage.StoreObject(lifeCycleOEMProductionFirmwareStatusPath, lifeCycleOEMProductionFirmwareImageStatus)

	p.JsonResponse(http.StatusNotImplemented, json.RawMessage(`{"error": "not implemented"}`))

	// update status
	lifeCycleOEMProductionFirmwareImageStatus.State = lifecycleStatus.FINISHED
	filestorage.StoreObject(lifeCycleOEMProductionFirmwareStatusPath, lifeCycleOEMProductionFirmwareImageStatus)
}

func LifeCycle_FirmwareProductionImage_Get(p periHttp.PeriHttp) {
	binaryJson, err := json.Marshal(lifeCycleOEMProductionFirmwareImageStatus)
	if err != nil {
		p.JsonResponse(http.StatusInternalServerError, json.RawMessage(`{"error": "`+err.Error()+`"}`))
		return
	}

	p.JsonResponse(http.StatusOK, binaryJson)
}

func LifeCycle_FirmwareUpdateImage_Set(p periHttp.PeriHttp) {
	// update status
	lifeCycleUpdateFirmwareImageStatus.State = lifecycleStatus.RECEIVING
	filestorage.StoreObject(lifeCycleUpdateFirmwareStatusPath, lifeCycleOEMProductionFirmwareImageStatus)

	p.JsonResponse(http.StatusNotImplemented, json.RawMessage(`{"error": "not implemented"}`))

	// update status
	lifeCycleUpdateFirmwareImageStatus.State = lifecycleStatus.FINISHED
	filestorage.StoreObject(lifeCycleUpdateFirmwareStatusPath, lifeCycleOEMProductionFirmwareImageStatus)
}

func LifeCycle_FirmwareUpdateImage_Get(p periHttp.PeriHttp) {
	binaryJson, err := json.Marshal(lifeCycleUpdateFirmwareImageStatus)
	if err != nil {
		p.JsonResponse(http.StatusInternalServerError, json.RawMessage(`{"error": "`+err.Error()+`"}`))
		return
	}

	p.JsonResponse(http.StatusOK, binaryJson)
}

func LifeCycle_Reboot_Set(p periHttp.PeriHttp) {
	p.EmptyResponse(http.StatusAccepted)

	lifeCycle_cache.RebootCounter = lifeCycle_cache.RebootCounter + 1
	filestorage.StoreObject(lifeCycleInfoPath, lifeCycle_cache)

	go func() {
		time.Sleep(time.Second * 2)
		syscall.Reboot(syscall.LINUX_REBOOT_CMD_RESTART)
	}()
}

func LifeCycle_FactoryReset_Set(p periHttp.PeriHttp) {
	p.JsonResponse(http.StatusNotImplemented, json.RawMessage(`{"error": "not implemented"}`))
}

func LifeCycle_Config_Get(p periHttp.PeriHttp) {
	binaryJson, err := json.Marshal(lifeCycleConfig_cache)
	if err != nil {
		p.JsonResponse(http.StatusInternalServerError, json.RawMessage(`{"error": "`+err.Error()+`"}`))
		return
	}

	p.JsonResponse(http.StatusOK, binaryJson)
}

func LifeCycle_Config_Set(p periHttp.PeriHttp) {
	payload, _ := p.ReadBody()
	newLifeCycleConfig := lifeCycleConfig_cache

	err := json.Unmarshal(payload, &newLifeCycleConfig)
	if err != nil {
		log.Println("NodeConfigSet error: ", err)
		p.EmptyResponse(http.StatusBadRequest)
		return
	}

	if lifeCycleConfig_cache != newLifeCycleConfig {
		lifeCycleConfig_cache = newLifeCycleConfig

		err = filestorage.StoreObject(lifeCycleConfigPath, lifeCycleConfig_cache)
		if err != nil {
			log.Println("error on storing lifeCycleConfig: ", err)
			p.EmptyResponse(http.StatusInternalServerError)
			return
		}

		// update node life_state
		json_data, err := json.Marshal(newLifeCycleConfig)
		if err != nil {
			intHttp.Put(node.LifeStateSet, json_data, nil)
		}
	}

	p.EmptyResponse(http.StatusNoContent)
}
