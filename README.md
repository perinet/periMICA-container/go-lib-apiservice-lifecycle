# API Sercice LifeCycle

The apiservice LifeCycle is the implementation of the lifeCycle part from the
[unified api](https://gitlab.com/perinet/unified-api). The api service lifeCycle is
available in all Perinets Products. 

This implementation does store its resources, e.g. NodeInfo object or the ProductionInfo object persistently in the file system `/var/lib/apiservice-lifeCycle/` as json files.

## References

* [openAPI spec for apiservice LifeCycle] https://gitlab.com/perinet/unified-api/-/blob/main/services/LifeCycle_openapi.yaml


# Dual License

This software is by default licensed via the GNU Affero General Public License
version 3. However it is also available with a commercial license on request
(https://perinet.io/contact).