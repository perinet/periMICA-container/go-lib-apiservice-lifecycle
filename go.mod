module gitlab.com/perinet/periMICA-container/apiservice/lifecycle

go 1.22

toolchain go1.22.6

require (
	gitlab.com/perinet/generic/lib/httpserver v1.0.1-0.20240925125014-00b1c681eff0
	gitlab.com/perinet/generic/lib/utils v1.0.1-0.20240621122644-d12809863382
	gitlab.com/perinet/periMICA-container/apiservice/node v1.0.4
	gotest.tools/v3 v3.5.1
)

require (
	github.com/alexandrevicenzi/go-sse v1.6.0 // indirect
	github.com/felixge/httpsnoop v1.0.3 // indirect
	github.com/google/go-cmp v0.5.9 // indirect
	github.com/google/uuid v1.6.0 // indirect
	github.com/gorilla/handlers v1.5.2 // indirect
	github.com/gorilla/mux v1.8.1 // indirect
	golang.org/x/exp v0.0.0-20240325151524-a685a6edb6d8 // indirect
)
