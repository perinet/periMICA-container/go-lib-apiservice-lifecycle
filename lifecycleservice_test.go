/*
Copyright (c) 2018-2022 Perinet GmbH
All rights reserved

This software is dual-licensed: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3 as
published by the Free Software Foundation. For the terms of this
license, see http://www.fsf.org/licensing/licenses/agpl-3.0.html,
or contact us at https://perinet.io/contact/ when you want license
this software under a commercial license.
*/

package lifecycle

import (
	"encoding/json"
	"net/http"
	"os"
	"testing"

	"gitlab.com/perinet/generic/lib/utils/filestorage"
	"gitlab.com/perinet/generic/lib/utils/intHttp"
	"gitlab.com/perinet/periMICA-container/apiservice/lifecycle/lifecycleStatus"
	"gitlab.com/perinet/periMICA-container/apiservice/node/nodeLifeState"
	"gotest.tools/v3/assert"
)

func TestLifeCycleInfo(t *testing.T) {
	var info LifeCycleInfo
	data := intHttp.Get(LifeCycle_Info_Get, nil)
	err := json.Unmarshal(data, &info)

	assert.NilError(t, err)
	assert.Assert(t, info.ApiVersion == "23")
}

func TestFirmwareProductionImage(t *testing.T) {
	resp := intHttp.Call(LifeCycle_FirmwareProductionImage_Set, "PATCH", nil, nil)

	assert.Assert(t, resp.StatusCode == http.StatusNotImplemented)
}

func TestFirmwareUpdateImage(t *testing.T) {
	resp := intHttp.Call(LifeCycle_FirmwareUpdateImage_Set, "PATCH", nil, nil)

	assert.Assert(t, resp.StatusCode == http.StatusNotImplemented)
}

// TODO mock reboot call
// func TestReboot(t *testing.T) {
//  resp := intHttp.Call(LifeCycle_Reboot_Set, "PATCH", nil, nil)
//
// 	assert.Assert(t, resp.StatusCode == http.StatusAccepted)
// }

func TestReset(t *testing.T) {
	resp := intHttp.Call(LifeCycle_FactoryReset_Set, "PATCH", nil, nil)

	assert.Assert(t, resp.StatusCode == http.StatusNotImplemented)
}

func TestLifeCycleConfigStorage(t *testing.T) {
	var err error

	os.Remove(lifeCycleConfigPath)

	lifeCycleConfig_cache.State = nodeLifeState.PRODUCTION

	filestorage.StoreObject(lifeCycleConfigPath, lifeCycleConfig_cache)
	assert.Assert(t, err == nil)

	newLifeCycleConfig_cache := defaultLifeCycleConfig

	// load back
	filestorage.LoadObject(lifeCycleConfigPath, &newLifeCycleConfig_cache)
	assert.Assert(t, err == nil)

	assert.Assert(t, lifeCycleConfig_cache.State == nodeLifeState.PRODUCTION)

	lifeCycleConfig_cache.State = nodeLifeState.UPDATED

	data, err := json.Marshal(lifeCycleConfig_cache)
	intHttp.Patch(LifeCycle_Config_Set, data, nil)
	assert.Assert(t, err == nil)

	data1 := intHttp.Get(LifeCycle_Config_Get, nil)
	var newLC LifeCycleConfig
	err = json.Unmarshal(data1, &newLC)
	assert.Assert(t, err == nil)
	assert.Assert(t, newLC.State == nodeLifeState.UPDATED)
}


func TestLifeCycleFirmwareProductionImage(t *testing.T) {
	var err error

	data1 := intHttp.Get(LifeCycle_FirmwareProductionImage_Get, nil)
	var status LifeCycleOEMProductionFirmwareImageStatus
	err = json.Unmarshal(data1, &status)
	assert.Assert(t, err == nil)
	assert.Assert(t, status.State == lifecycleStatus.FINISHED )  //it has already been tested
}

func TestLifeCycleFirmwareUpdateImage(t *testing.T) {
	var err error

	data1 := intHttp.Get(LifeCycle_FirmwareUpdateImage_Get, nil)
	var status LifeCycleOEMProductionFirmwareImageStatus
	err = json.Unmarshal(data1, &status)
	assert.Assert(t, err == nil)
	assert.Assert(t, status.State == lifecycleStatus.FINISHED ) //it has already been tested
}